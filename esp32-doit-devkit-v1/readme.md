
# 商品簡介
https://www.botsheet.com/cht/shop/esp-wroom-32/

ESP-WROOM-32基於 ESP-32所設計的開發板。延續Espressif之前推出的ESP8266，是相容於 802.11 b/g/n/e/i 2.4 GHz Wi-Fi 的微控制器，再加入藍牙 v4.2 BR/EDR 和低功耗藍牙（BLE、BT4.0、Bluetooth Smart）以及引出 ESP-32所有的GPIO，配有雙核心 Tensilica 32bit LX6 微處理器，高達 240 MHz 時脈頻率，開發者可以使用面包板進行開發和調試時。

ESP32 具有功率效能、RF 效能、耐用度、多功能性、特點以及可靠性，因此非常適合 IoT 和連網專案。能安裝 ESP32 Arduino Core，即可透過 Arduino IDE 進行編程。


# ESP32 with Arduino IDE --- first you need !
https://lastminuteengineers.com/esp32-arduino-ide-tutorial/

# ESP32 Pinout Reference: Which GPIO pins should you use?
https://randomnerdtutorials.com/esp32-pinout-reference-gpios/

# Getting Started with the ESP32 Development Board
https://randomnerdtutorials.com/getting-started-with-esp32/

# ESP32 ADC – Read Analog Values with Arduino IDE
https://randomnerdtutorials.com/esp32-adc-analog-read-arduino-ide/



# 中文資源

## NodeMcu DOIT ESP32 DEVKIT V1
https://smartleeaudio.blogspot.com/2019/02/nodemcu.html