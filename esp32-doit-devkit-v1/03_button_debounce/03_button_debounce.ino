/*
 * 按下按鈕後才跳出 Hello world
 */

int pushButton = 2;
int buttonState = 0;
int beforeState = 0;
int count = 0;

void setup() {
  Serial.begin(115200);
  pinMode(pushButton, INPUT);
}

void loop() {
  buttonState = digitalRead(pushButton);
  if (buttonState == 1 && beforeState == 0) {
    count = count + 1;
    Serial.print("Count : ");
    Serial.println(count);
  }

  // Debounce
  if (buttonState != beforeState){
    delay(20);
  }
  
  beforeState = buttonState;
  delay(1);      
}
