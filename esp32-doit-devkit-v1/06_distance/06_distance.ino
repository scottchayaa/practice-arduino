/*
 * 超音波測距
 *  - HC-SR04 模組 * 1
 */

int trigPin = 2;                    //Trig Pin (digital)
int echoPin = 5;                    //Echo Pin (digital)
long duration;
float cm;
 
void setup() {
  Serial.begin (9600);              // Serial Port begin
  pinMode(trigPin, OUTPUT);         // Define inputs and outputs 
  pinMode(echoPin, INPUT);
}
 
void loop()
{
  // Clear trigPin (reset)
  digitalWrite(trigPin, LOW);           
  delayMicroseconds(5);                 // 應該是 debounce

  // 給 Trig 高電位，持續 10微秒
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);

  /*
   * 音速公尺/秒 換算成 公分/微秒：343.64 * 100 / 1000000 = 0.034364 公分/微秒，亦即
   * 音速每公分需要29.1 微秒：  1 / 0.034364 = 29.1 微秒/公分
   */
  duration = pulseIn(echoPin, HIGH);      // 收到高電位時的時間 (微秒)
  cm = (duration / 2) / 29.1;             // 將時間換算成距離 cm  

  Serial.print("duration : ");  
  Serial.print(duration);
  Serial.print(" µs, ");
  Serial.print("distance : ");  
  Serial.print(cm);
  Serial.print(" cm");
  Serial.println();
  
  delay(200);
}
